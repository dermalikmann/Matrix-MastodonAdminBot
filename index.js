const sdk = require("matrix-bot-sdk");
const MatrixClient = sdk.MatrixClient;
const SimpleFsStorageProvider = sdk.SimpleFsStorageProvider;
const AutojoinRoomsMixin = sdk.AutojoinRoomsMixin;
var fs = require('fs');

var _secrets = fs.readFileSync('secrets.json');

const storage = new SimpleFsStorageProvider("bot.json");
const client = new MatrixClient(homeserverUrl, matrixAccessToken, storage);
AutojoinRoomsMixin.setupOnClient(client);

client.start().then(() => console.log("Client started!"));

client.on("room.message", (roomId, event) => {
	if (!event["content"]) return;
	const body = event["content"]["body"].trim().toLowerCase();
	if (body.starsWith("!listreports")) {
		//do stuff
	}
	else if (body.starsWith("!listhashtags")) {
		// do stuff
	}
	else if (body.starsWith("!listaccounts")) {
		//do stuff
	}
	else if (body.starsWith("!help")) {
		client.sendMessage(roomId, {
			"msgtype": "m.text",
			"body": "This is the help page for the Mastodon admin bot.\n Available Commands are:\n- **!help** to display this page\n- **!listreports** to display all open reports\n- **!isthashtags** to display all hashtags awaiting confirmation\n- **!listaccounts** to display all accounts awaiting confirmation "
		})
	}
});